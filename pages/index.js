import Head from "next/head";
import Link from "next/link";
import styles from "../styles/Home.module.css";

export default function Home() {
  return (
    <>
      <Head>
        <title>Ninja List | Home</title>
        <meta name="keywords" content="ninjas" />
      </Head>
      <div>
        <h1 className={styles.title}>Homepage</h1>
        <p className={styles.text}>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Corrupti quo neque perspiciatis, ipsum repudiandae et ipsam iste odit error sit.
          Doloremque sequi sed qui voluptas enim consectetur quia aspernatur porro?
        </p>
        <p className={styles.text}>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Corrupti quo neque perspiciatis, ipsum repudiandae et ipsam iste odit error sit.
          Doloremque sequi sed qui voluptas enim consectetur quia aspernatur porro?
        </p>
        <Link href="/ninjas">
          <a className={styles.btn}>Ninja Listing</a>
        </Link>
      </div>
    </>
  );
}

import Head from "next/head";

const About = () => {
  return (
    <>
      <Head>
        <title>Ninja List | About</title>
        <meta name="keywords" content="ninjas" />
      </Head>
      <div>
        <h2>About us</h2>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Eos excepturi aliquam reiciendis debitis repellat, ducimus tenetur est corporis rem
          placeat maxime, necessitatibus, mollitia autem nam provident officia sunt fugiat. Illo.
        </p>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Eos excepturi aliquam reiciendis debitis repellat, ducimus tenetur est corporis rem
          placeat maxime, necessitatibus, mollitia autem nam provident officia sunt fugiat. Illo.
        </p>
      </div>
    </>
  );
};

export default About;
